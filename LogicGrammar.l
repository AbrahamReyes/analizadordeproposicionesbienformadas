%{
#include "LogicGrammar.tab.h"
%}

VARIABLE [a-z]
CONECTIVEOR 	OR
CONECTIVEAND  	AND
NEGATION		NOT
IMPLICATION 	THEN
CIFF				IFF
PAROP     		\(
PARCL			\)
EOLS 			[\n]
%%
{VARIABLE}		{yylval.sv = strdup(yytext); return VAR;}
{CONECTIVEOR}	{yylval.sv = strdup(yytext); return OR;}
{CONECTIVEAND}	{yylval.sv = strdup(yytext); return AND;}
{NEGATION}		{yylval.sv = strdup(yytext);return NOT;}
{IMPLICATION}	{yylval.sv = strdup(yytext); return THEN;}
{CIFF}			{yylval.sv = strdup(yytext); return IFF;}
{PAROP}			{yylval.sv = strdup(yytext); return A;}
{PARCL}			{yylval.sv = strdup(yytext); return C;}
{EOLS}			{yylval.sv = strdup(yytext); return EOL;}
"For all "		{yylval.sv = strdup(yytext); return UNQ;}
%%

