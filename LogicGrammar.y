/*Autor: Reyes Almanza Jesus Abraham*/

%{
#include <stdio.h>
#include "component.h"
#include "ast_rutinas.c"
#include "generarValoresBinarios.c"
int vars =0;
CList mylist = NULL;
struct ast *proposicion;
	int n = 0;
	int m = 0;
	 int *matriz;
	
%}
%locations
%union {char *sv; struct ast *a;}; 
%token <sv>VAR
%token <sv>OR
%token <sv>AND
%token <sv>NOT
%token <sv>THEN
%token <sv>IFF
%token <sv>A
%token <sv>C
%token EOL
%token UNQ

%type <a> prop prop1 prop2 prop3
%%

varprop:
	   | varprop prop EOL { 
							proposicion = $2;
							printf("\n");
							printf("\nNumero de variables: %d\n", vars);
							system("echo -e \"\\e[36m TABLA DE VERDAD\\e[0m\"");
							showList(mylist,0, vars);
							IOSearch(proposicion);
							printf("\n");
							for(int k=0;k<vars;k++)printf("----");
							printf("\n");
							n = vars;//numero de variables en la proposicion
							m = diyPow(2,n);//numero de posibilidades
							//matriz de valores posibles
							matriz = (int *)malloc(n*m*sizeof(int));
							generarMatriz(matriz,n,m);
							//printMatriz(matriz,n,m);
							for(int i=0;i < m;i++)
							 printf("  %d\n",eval(proposicion,&matriz[i*n],mylist,vars));

							treefree(proposicion);
							delete_list(&mylist);
							vars=0;
							system("echo -e \"\\e[3mIntroduzcan una nueva proposición o pulse crtl+c para salir...\\e[0m\"");
							
						 }
prop : VAR {
	 			$$ = newast($1,NULL,NULL);
				if(existePorNombre(mylist,$1) == 0){
					vars++;
					insertVariable(&mylist,vars,$1);		
				}
		   }
	 | prop1 
	 | prop2
	 | prop3
prop1: NOT  prop  { $$ = newast($1, NULL,$2); }
	 | NOT A prop C { $$ = newast($1, NULL, $3);}
	 | NOT A prop2 C {$$ = newast($1,NULL,$3);}
	 | NOT A prop3 C {$$ = newast($1,NULL,$3);}
	 ;
prop2: VAR AND VAR {
	 					$$ = newast($2, newast($1,NULL,NULL), newast($3,NULL,NULL));	
						if(existePorNombre(mylist,$1) == 0){
							vars++;
							insertVariable(&mylist,vars,$1);		
						}
						if(existePorNombre(mylist,$3) == 0){
							vars++;
							insertVariable(&mylist,vars,$3);		
						
				   		}
				   }	
	 | VAR AND A prop C {
							$$ = newast($2, newast($1,NULL,NULL), $4);
							if(existePorNombre(mylist,$1) == 0){
								vars++;
								insertVariable(&mylist,vars,$1);		
							}
						}
	 | A prop C AND VAR {
							
							$$ = newast($4, $2, newast($5,NULL,NULL));
							if(existePorNombre(mylist,$5) == 0){
								vars++;
								insertVariable(&mylist,vars,$5);		
							}
						}
	 | A prop C AND A prop C {$$=newast($4, $2, $6);}
	 | VAR OR VAR {
	 					$$ = newast($2, newast($1,NULL,NULL), newast($3,NULL,NULL));	
				  		if(existePorNombre(mylist,$1) == 0){
							vars++;
							insertVariable(&mylist,vars,$1);		
						}
						if(existePorNombre(mylist,$3) == 0){
							vars++;
							insertVariable(&mylist,vars,$3);		
						
				   		}
				 }
	 | VAR OR A prop C {
							$$ = newast($2, newast($1,NULL,NULL), $4);
							if(existePorNombre(mylist,$1) == 0){
							vars++;
							insertVariable(&mylist,vars,$1);		
						}
					   }
	 | A prop C OR VAR {
							$$ = newast($4, $2, newast($5,NULL,NULL));
							if(existePorNombre(mylist,$5) == 0){
								vars++;
								insertVariable(&mylist,vars,$5);		
							}
					   }
	 | A prop C OR A prop C {$$=newast($4, $2, $6);};
	 ;
prop3: VAR THEN VAR {
	 					$$ = newast($2, newast($1,NULL,NULL), newast($3,NULL,NULL));	
						if(existePorNombre(mylist,$1) == 0)
						{
							vars++;
							insertVariable(&mylist,vars,$1);		
						}
						if(existePorNombre(mylist,$3) == 0){
							vars++;
							insertVariable(&mylist,vars,$3);
						}
					}
	 | VAR THEN A prop C {
							$$ = newast($2, newast($1,NULL,NULL), $4);
							printf("Variable implica Proposición\n");
							if(existePorNombre(mylist,$1) == 0)
							{
								vars++;
								insertVariable(&mylist,vars,$1);		
							}
						 }
	 | A prop C THEN VAR {
							$$ = newast($4, $2, newast($5,NULL,NULL));
							printf("Proposición implica Variable\n");
							if(existePorNombre(mylist,$5) == 0){
								vars++;
								insertVariable(&mylist,vars,$5);
							}
						 }
	 | A prop C THEN A prop C {$$=newast($4,$2,$6);}
	 | VAR IFF VAR {
	 					$$ = newast($2, newast($1,NULL,NULL), newast($3,NULL,NULL));	
						if(existePorNombre(mylist,$1) == 0)
						{
							vars++;
							insertVariable(&mylist,vars,$1);		
						}
						if(existePorNombre(mylist,$3) == 0){
							vars++;
							insertVariable(&mylist,vars,$3);
						}
				   }
	 | VAR IFF A prop C {
							$$ = newast($2, newast($1,NULL,NULL), $4);
							if(existePorNombre(mylist,$1) == 0)
							{
								vars++;
								insertVariable(&mylist,vars,$1);		
							}

						}
	 | A prop C IFF VAR {
							$$ = newast($4, $2, newast($5,NULL,NULL));
							if(existePorNombre(mylist,$5) == 0){
								vars++;
								insertVariable(&mylist,vars,$5);
							}
						}
	 | A prop C IFF A prop C {$$=newast($4,$2,$6);}
 
%%
main(int argc, char **argv)
{
system("clear");
system("echo -e \" \\e[1m****BIENVENIDO AL PROGRAMA DE PROPOSICIONES LÓGICAS****\\e[0m\"");
printf("\n");
system("echo -e \"\\e[3mPara escribir una proposición lógica debe seguir la siguiente sintaxis:\\e[0m\"");
printf("\n");
printf("--Variables de la \"a\" a la \"z\"\n");
printf("--Conectivos lógicos: AND, OR, NOT, THEN, IFF\n");
printf("--Nota: Se permite el uso paréntesis\n");
printf("Ejemplo: (x AND r) OR y \n");
printf("\n");
printf("Introduzca la proposicion lógica:\n");
yyparse();
}
yyerror(char *s)
{
fprintf(stderr, "error: %s\n", s);
}
