/*Archivo para crear la estructura del parser tree de una proposicion*//*Autor: Reyes Almanza Jesus Abraham*/

extern int yylineno;
/*Estructura del arbol*/
struct ast{
	char *name;
    int bit;
	struct ast *l;
	struct ast *r;
	
};
/**/
struct numval{
	int nodetype;
	char *var;
	int bit;
};

struct ast *newast(char *newName, struct ast *l, struct ast *r);
struct ast *newnum(char *newVar);

/*Evaluacion*/
int eval(struct ast*, int *listaValores, CList proposicion, int n);

/*Borrar y liberar un AST*/
void treefree(struct ast *);


