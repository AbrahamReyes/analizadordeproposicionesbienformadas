/*
Autor: Reyes Almanza Jesus Abraham
 */
#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
int then_val(int a, int b){
	int t=1;
	if(a==1 && b == 0)
		t = 0;	
	return t;
}
int iff_val(int a, int b){
	int t=0;
	if(a==b)
		t=1;
	return t;
}
struct ast* newast(char *newName, struct ast *l, struct ast *r){
	struct ast *a = malloc(sizeof(struct ast));
    /*if(!a) {
		yyerror("out of space");
		exit(0);
	}*/		
	a->name = newName;
	a->l = l;
	a->r = r;
	return a;
}
struct ast *newnum(char *newVar)
{
 struct numval *a = malloc(sizeof(struct numval));
 	if(!a) {
	//	yyerror("out of space");
		exit(0);
	}
	a->nodetype = 'VAR';
	a->var = newVar;
	return (struct ast *)a;
}

int eval(struct ast *a, int *lValuaciones, CList lVariables, int n)
{
 if(a!=NULL){
	int v,x,y,id; //Calcular el valor del subarbol
	char *opc = a->name;
		if(existePorNombre(lVariables,opc)){
			//Buscamos la variable correspondiente en la lista y devolvemos el id 
			id = getVariable(lVariables,a->name)->id;
			//printf("BUSCA LA VARIABLE %s con id %d\n",opc, id);
			for(int j=0;j<n;j++){
				if(j==(id-1)){
				 v=lValuaciones[j];
				 printf(" %d |", lValuaciones[j]);
				}
			}
		}	
			
		if(!strcmp(opc,"NOT")){
			 v = eval(a->r,lValuaciones,lVariables,n); 
			 if(v==1) v = 0;
			 else if(v==0)v=1;
		}
		if(!strcmp(opc,"AND")){ 
			//printf("Entro al AND \n");
			v = eval(a->l,lValuaciones,lVariables,n) & eval(a->r,lValuaciones,lVariables,n);
		}
		if(!strcmp(opc,"OR")) 
		v = eval(a->l,lValuaciones,lVariables,n) | eval(a->r,lValuaciones,lVariables,n);
		if(!strcmp(opc,"THEN")){
				  	x = eval(a->l,lValuaciones,lVariables,n);
				   	y = eval(a->r,lValuaciones,lVariables,n);
					v = then_val(x,y);
		}

		if(!strcmp(opc,"IFF")){
					x = eval(a->l,lValuaciones,lVariables,n);
				   	y = eval(a->r,lValuaciones,lVariables,n);
					v = iff_val(x,y);
		}
	
	return v;
 }

}

void treefree( struct ast *a)
{
	if(a->name=="AND" || a->name=="OR" || a->name=="THEN" || a->name=="IFF")
			treefree(a->r);
	if(a->name=="NOT")
		treefree(a->l);
	if(a->name=="VAR")
		free(a);
	
}
void IOSearch(struct ast *a){
	if(a != NULL){
		IOSearch(a->l);
		printf(" %s ", a->name);
		IOSearch(a->r);
	}
}
