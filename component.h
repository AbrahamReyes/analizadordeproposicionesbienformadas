#ifndef UPWARD
#define UPWARD 0
#define DOWNWARD 1
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#endif
//Structure for logic Variables
typedef struct Variable{

	struct Variable *next;
	int id;
	char *Nombre;
	struct Variable *previous;
}roleVariable;
typedef roleVariable *pVariable;
typedef roleVariable *CList;

void insertVariable(CList *mylist, int newId, char *newNombre){
	pVariable newC, actual;
	
	//Creating new Variable
	newC = (pVariable)malloc(sizeof(roleVariable));
	newC->id = newId;
	newC->Nombre = newNombre;
	//setting the actual Variable to the list
	actual = *mylist;

	//set actual to the first element of the list
	if(actual)while(actual->previous)actual=actual->previous;
	
	//Empty CList of Variables
	if(!actual){
		newC->next = actual;
		newC->previous = NULL;
		if(!*mylist) *mylist = newC;
	}
	else{
		/*Going forward until the last element is reached*/
		while(actual->next)actual = actual->next;
			newC->next = actual->next;
			actual->next = newC;
			newC->previous = actual;
		
	
	}
	//printf("\n Se inserto Variable \n");
}
void delete_list(CList *mylist){
	pVariable xVariable, actual;

	actual = *mylist;

	while(actual){
		xVariable = actual;
		actual = actual->next;
		free(xVariable);
	}	
	*mylist=NULL;
}	
	
void showList(CList mylist, int order, int vars){
	pVariable xVariable;
	xVariable = mylist;
	if(!mylist){ printf("Error there is no electric Variables\n"); exit(1);}
	
	if(order==UPWARD){
		while(xVariable->previous)xVariable = xVariable->previous;
		while(xVariable){
			//printf("id : %d ", xVariable->id);
			printf(" %s |", xVariable->Nombre);
			xVariable = xVariable->next;
		}
		//printf("\n");
 	//	printf("\n");
		
	}
	else if(order==DOWNWARD){
		while(xVariable->next)xVariable = xVariable->next;
		while(xVariable){
			printf("id : %d ", xVariable->id);
			printf("Nombre : %s \n", xVariable->Nombre);
			xVariable=xVariable->previous;
		}
		
	}	
}

// devuelve 0 si lista vacia
// devuelve 1 de lo contrario
int isEmpty(CList mylist)
{
	pVariable xVariable;
	xVariable = mylist;
	if(!mylist) return 1;
	return 0;
}

int existePorNombre(CList mylist, char *xNombre){
        pVariable xVariable;
        xVariable=mylist;
		//printf("Nombre a buscar %s\n", xNombre);
        //while(xVariable)xVariable = xVariable->previous;
		while(xVariable){
			//printf("Valor acutal de la busqueda %s\n", xVariable->Nombre);	
        	if(strcmp((xVariable->Nombre),xNombre) == 0)
				return 1;
            xVariable= xVariable->next;
				
        }
		//printf("no se encontro variable\n");
        return 0;
}	
// Revisa que un Variablee no este repetido
// Regresa 1 si encontro ese Variablee repetido
// Regresa 0 si no encontro ese Variablee repetido
pVariable getVariable(CList mylist, char *xNombre)
{
 pVariable xVariable;
        xVariable=mylist;
		//printf("Nombre a buscar %s\n", xNombre);
        //while(xVariable)xVariable = xVariable->previous;
		while(xVariable){
			//printf("Valor acutal de la busqueda %s\n", xVariable->Nombre);	
        	if(strcmp((xVariable->Nombre),xNombre) == 0)
				return xVariable;
            xVariable= xVariable->next;
				
        }
		//printf("no se encontro variable\n");
        return NULL;

}
