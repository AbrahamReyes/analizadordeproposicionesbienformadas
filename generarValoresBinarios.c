//Autor: Reyes Almanza Jesus Abraham
//
#include <stdio.h>
#include <stdlib.h>
int diyPow(int base, int power){
	int r=base;
	for (int i=1;i<power;i++){
		r *= base;
	}
	return r;
}
void printMatriz(int *matriz, int n, int m){
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			printf(" %d |", matriz[j+i*n]);
		}
		printf("\n");
	}
}
void generarMatriz(int *matriz, int n, int m){
	int cos =0, res=0, div=0;

	for (int i=0;i<m;i++){
			div = i;
		for(int j=1;j<=n;j++){
			cos = div/2;
			res = div % 2;
		    matriz[(j-1)+i*(n)]=res;
		  div = cos;
		}
	}
}
